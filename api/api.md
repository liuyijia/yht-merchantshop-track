
1. 添加商品
    请求地址 : http://shop.yht.org/api/good_add.php
    请求方式 : POST
    请求参数 :
        goods_sn（必须）    ： 商品唯一货号 （varchar(60)）
        goods_name（必须）  ： 商品名称 （varchar(120)）
        goods_brief        ： 商品简介 （varchar(255)）
        goods_desc（必须）  ： 商品详细描述 （text）
        original_img（必须）： 上传的商品的原始图片url地址 （varchar(255)）
        goods_weight       ： 商品的重量，以千克为单位 （decimal(10,3)）
        market_price（必须）： 市场售价 （decimal(10,2)）
        shop_price         ： 本店售价 （decimal(10,2)）
        is_on_sale         ： 该商品是否开放销售，上架 （tinyint(1) 1，是，默认；0，否）
    返回结果 :
        err_code ： 错误返回码（int）

2. 修改商品信息
      请求地址 : http://shop.yht.org/api/good_update.php
      请求方式 : POST
      请求参数 :
          goods_sn（必须）： 商品唯一货号 （varchar(60)）|必须
          goods_name     ： 商品名称 （varchar(120)）
          goods_brief    ： 商品简介 （varchar(255)）
          goods_desc     ： 商品详细描述 （text）
          original_img   ： 上传的商品的原始图片url地址 （varchar(255)）
          goods_weight   ： 商品的重量，以千克为单位 （decimal(10,3)）
          market_price   ： 市场售价 （decimal(10,2)）
          shop_price     ： 本店售价 （decimal(10,2)）
          is_on_sale     ： 该商品是否开放销售，上架 （tinyint(1)）。1，是；0，否
      返回结果 :
          err_code ： 错误返回码（int）
          
3. 商品上架 | 商品下架
      请求地址 : http://shop.yht.org/api/good_on_sale.php
      请求方式 : POST
      请求参数 :
          goods_sn（必须）： 商品唯一货号 （varchar(60)）| 必须
          is_on_sale     ： 商品上下架 （tinyint(1)）。1，上架；0，下架
      返回结果 :
          err_code ： 错误返回码（int）
          
4. 获取订单信息
      请求地址 : http://shop.yht.org/api/order_info.php
      请求方式 : GET
      请求参数 :
          add_time ： 订单创建时间 （格林威治时间戳, int(11)） | 可选
          order_id ： 订单ID | 可选
      返回结果 :
          err_code       ： 错误返回码（int）
          order_id       ：	mediumint(8) | 订单ID
          order_sn       ：	varchar(20) | 订单号,唯一
          order_status   ：	tinyint(1) | 订单的状态;0未确认,1确认,2已取消,3无效,4退货
          shipping_status： tinyint(1) | 商品配送情况;0未发货,1已发货,2已收货,4退货
          pay_status     ： tinyint(1) | 支付状态;0未付款;1付款中;2已付款
          consignee      ： varchar(60) | 收货人的姓名
          address        ：	varchar(255) | 收货人的详细地址
          zipcode        ：	varchar(60) | 收货人的邮编
          tel            ：	varchar(60) | 收货人的电话
          mobile         ：	varchar(60) | 收货人的手机
          email          ：	varchar(60) | 收货人的Email
          postscript     ：	varchar(255) | 订单附言,由用户提交订单前填写
          how_oos        ：	varchar(120) | 缺货处理方式,等待所有商品备齐后再发,取消订单;与店主协商
          inv_payee      ：	varchar(120) | 发票抬头,用户页面填写
          inv_content    ：	varchar(120) | 发票内容
          goods_amount   ：	decimal(10,2) | 商品的总金额
          order_amount   ：	decimal(10,2) | 应付款金额
          add_time       ：	int(10) | 订单生成时间
          confirm_time   ：	int(10) | 订单确认时间
          pay_time       ：	int(10) | 订单支付时间
          to_buyer       ：	varchar(255) | 商家给客户的留言,当该字段值时可以在订单查询看到
          pay_note       ：	varchar(255) | 付款备注, 在订单管理编辑修改

5. 获取订单商品明细
      请求地址 : http://shop.yht.org/api/order_detail.php
      请求方式 : GET
      请求参数 :
          order_id ： 订单ID  | 必须
      返回结果 :
          err_code       ： 错误返回码（int）
          order_id       ： mediumint(8) | 订单ID 
          goods_name     ： varchar(120) | 商品的名称
          goods_sn       ： varchar(60) | 商品的唯一货号
          goods_number   ： smallint(5)	| 商品的购买数量
          market_price   ： decimal(10,2) | 商品的市场售价
          goods_price    ： decimal(10,2) | 商品的本店售价
          
6. 获取订单状态更新
      请求地址 : http://shop.yht.org/api/order_status.php
      请求方式 : GET
      请求参数 :
          updated_time ： 订单更新时间 （格林威治时间戳, int(11)） | 可选
          order_id ： 订单ID | 可选
      返回结果 :
          err_code       ： 错误返回码（int）
          order_id       ： mediumint(8) | 订单ID 
          order_status   ： tinyint(1) | 订单状态。 0未确认; 1已确认; 2已取消; 3无效; 4退货
          shipping_status： tinyint(1) | 发货状态。 0未发货; 1已发货; 2已取消; 3备货中
          pay_status     ： tinyint(1) | 支付状态。 0未付款; 1已付款中; 2已付款
          updated_time   ： int(11) | 操作时间