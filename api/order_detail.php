<?php
include 'common.php';

if(empty($_GET)){
    echo json_encode(array('err_code'=>102));

}else{
    if(isset($_GET['order_id'])){
        include './db.php';

        if($pdo){
            $sql = 'SELECT G.order_id, G.goods_name, G.goods_sn, G.goods_number, G.market_price, G.goods_price FROM '.$prefix.'order_goods G WHERE G.order_id=?';

            $data = sql_query_all($pdo, $sql, array($_GET['order_id']));

            if(false === $data){
                echo json_encode(array('err_code'=>-1));    // 数据库查询出错
            } else {
                echo json_encode(array('err_code'=>0, 'data'=>$data));
            }
        }else{
            echo json_encode(array('err_code'=>101));
        }

    }else{
        echo json_encode(array('err_code'=>102));
    }
}
