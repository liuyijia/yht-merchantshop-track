<?php
include 'common.php';

if(empty($_POST)){
    echo json_encode(array('err_code'=>102));

}else{
    include './db.php';

    if(isset($_POST['goods_sn'])){

        // 检查货号是否重复
        $sql = 'SELECT COUNT(*) counts FROM `'.$prefix.'goods` WHERE goods_sn=? AND is_delete = 0';
        $data = sql_query_one($pdo, $sql, array($_POST['goods_sn']));

        if(false === $data){
            echo json_encode(array('err_code'=>-1, 'msg'=>'数据库查询出错'));

        }elseif('1' == $data['counts']) {
            echo json_encode(array('err_code'=>202, 'msg'=>'货号存在'));

        }else{
            /* 字段默认值 */
            $default_value = array(
                'goods_sn' => '',           // 商品唯一货号
                'goods_name' => '',         // 商品名称
                'goods_brief' => '',        // 商品简介
                'goods_desc' => '',         // 商品详细描述
                'goods_thumb' => '',        // 商品在前台显示的微缩图片，如在分类筛选时显示的小图片
                'goods_img' => '',          // 商品的实际大小图片，如进入该商品页时介绍商品属性所显示的大图片
                'original_img' => '',       // 上传的商品的原始图片url地址

                'brand_id'      => 0,       // 品牌id
                'goods_number'  => 0,       // 商品库存数量
                'goods_weight'  => 0,       // 商品的重量，以千克为单位
                'market_price'  => 0,       // 市场售价
                'shop_price'    => 0,       // 本店售价
                'warn_number'   => 0,
                'is_real'       => 1,       // 是否是实物，1，是；0，否；比如虚拟卡就为0，不是实物
                'is_on_sale'    => 1,       // 该商品是否开放销售（上架），1，是；0，否
                'is_alone_sale' => 1,       // 是否能单独销售，1，是；0，否；如果不能单独销售，则只能作为某商品的配件或者赠品销售
                'integral'      => 0,
                'add_time'      => time(),
                'is_best'       => 0,
                'is_new'        => 1,
                'is_hot'        => 0,
                'goods_type'    => 0,
                'last_update'   => gmtime(),
            );

            $_fields = array();
            $_values = array();
            foreach($default_value as $field=>$value){
                $_fields[] = $field;
                $_values[] = '?';
            }
            $fields = implode(',', $_fields);
            $values = implode(',', $_values);

            $post = array();
            foreach($_POST as $f=>$v){
                if(in_array($f, $_fields)){
                    $post[$f] = $v;
                }
            }

            $stmt = $pdo->prepare("INSERT INTO `{$prefix}goods` ({$fields}) VALUES ({$values})");
            $_values = array_merge($default_value, $post);

            if($stmt->execute(array_values($_values))) {
                echo json_encode(array('err_code'=>0, 'goods_id'=>$pdo->lastInsertId(), 'msg'=>''));
                clear_cache_files();
            }else{
                echo json_encode(array('err_code'=>201, 'msg'=>'添加商品失败'));
            }

        }

    }else{
        echo json_encode(array('err_code'=>102, 'msg'=>'缺少必须参数'));
    }

}


