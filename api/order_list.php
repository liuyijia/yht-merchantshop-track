<?php
include 'common.php';

if(empty($_GET)){
    echo json_encode(array('err_code'=>102));

}else{
    include './db.php';

    if($pdo){
        $sql = 'SELECT order_id, order_sn, order_status, shipping_status, pay_status, consignee, address, zipcode, tel, mobile, email, postscript, how_oos, inv_payee, inv_content, goods_amount, order_amount, add_time, confirm_time, pay_time, to_buyer, pay_note FROM '.$prefix.'order_info ';

        $param = array();
        if(isset($_GET['order_id'])){
            $sql .= 'WHERE order_id=?';
            $param = array($_GET['order_id']);

        }elseif(isset($_GET['add_time'])){
            $sql .= 'WHERE add_time>?';
            $param = array($_GET['add_time']);

        }

        $data = sql_query_all($pdo, $sql, $param);

        if(false === $data){
            echo json_encode(array('err_code'=>-1));    // 数据库查询出错
        } else {
            echo json_encode(array('err_code'=>0, 'data'=>$data));
        }

    }else{
        echo json_encode(array('err_code'=>101));
    }

}