<?php
include 'common.php';

if(empty($_POST)){
    echo json_encode(array('err_code'=>102));

}else{
    include './db.php';

    $goods_sn = $_POST['goods_sn'];
    $is_on_sale = $_POST['is_on_sale'];

    $stmt = $pdo->prepare("UPDATE `{$prefix}goods` SET is_on_sale=?,last_update=? WHERE goods_sn=?");
    if($stmt->execute(array($is_on_sale, gmtime(), $goods_sn))) {
        echo json_encode(array('err_code'=>0, 'msg'=>''));
        clear_cache_files();
    }else{
        echo json_encode(array('err_code'=>203, 'msg'=>'操作失败'));
    }

}
